#!/bin/bash
#set -x

# These variables correspond to the inputs and parameter ids from the apps.json file. 
# The API will pass the actual values into this script prior to execution.
INPUT="/iplant/home/dooley/input1.txt" 
MY_INTEGER_PARAM=1
MY_ENUM_PARAM="val1"
MY_STRING_PARAM="alpha"

# Conditional create an input/output directory in the local scratch folder
if [ ! -d input ]; then mkdir input; fi
if [ ! -d output ]; then mkdir output; fi

# Copy from iRODS source to the input directory
iget -f -V -K -T $INPUT input/

# Figure out the filename that has been copied into input/
# Here, I use a clever trick for grabbing the last field of any slash-delim path - it's similar in 
# function to basename, but is a simple string operation rather than a system call. It can therefore
# work on URLs as well as iRODS paths and local filesystem paths.
#
# See http://tldp.org/LDP/LGNET/18/bash.html for more cool stuff like this.
#
FILENAME=${INPUT##*/} 

# Check for existence of input file...
if [ -e input/$FILENAME ]; then
	
	# Build up the arguments string
	ARGS=""
	
	if [ ${MY_INTEGER_PARAM} -ne 0 ]; then
		echo "myIntegerParameter configured"
		ARGS="${ARGS} -n ${MY_INTEGER_PARAM}"
	fi
	
	if [ -n ${MY_STRING_PARAM} ]; then
		echo "myStringParameter configured"
		ARGS="${ARGS} -c ${MY_STRING_PARAM}"
	fi
	
	if [ -z ${MY_ENUM_PARAM} ]; then
		echo "myEnumerationParameter not defined"
	elif [ ${MY_ENUM_PARAM} = "val1" ]; then
		echo "myEnumerationParameter defined configured"
		ARGS="${ARGS} --val1=${MY_ENUM_PARAM}"
	elif [ ${MY_ENUM_PARAM} = "val2" ]; then
		echo "myEnumerationParameter defined configured"
		ARGS="${ARGS} --val2=${MY_ENUM_PARAM}"
	elif [ ${MY_ENUM_PARAM} = "val3" ]; then
		echo "myEnumerationParameter defined configured"
		ARGS="${ARGS} --val3=${MY_ENUM_PARAM}"
	else
		echo "unrecognized myEnumerationParameter value"
		exit 1
	fi
	
	../bin/boilerplate $ARGS input/$FILENAME > output/boilerplate_output.txt

fi

# no need to stage out data, your output will be archived for you.

